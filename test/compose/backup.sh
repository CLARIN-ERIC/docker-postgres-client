#!/bin/bash

echo "$(pwd), compose_dir=${COMPOSE_DIR}"

. "${COMPOSE_DIR}/clarin/.env"
#See https://www.postgresql.org/docs/9.3/libpq-pgpass.html
echo "${DB_HOST}:${DB_PORT}:${DB_NAME}:${DB_USER}:${DB_PASSWORD}" > .pgpass
(cd "${COMPOSE_DIR}/clarin" && TIMESTAMP=${BACKUPS_TIMESTAMP} docker-compose -f docker-compose.yml -f docker-compose-backup.yml up --force-recreate backup)
rm .pgpass