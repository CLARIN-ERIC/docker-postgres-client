#!/bin/bash

set -e

ProgName=$(basename "$0")
_NICE_LEVEL=${NICE_LEVEL:-10}

PG_PASS_FILE="/pg/.pgpass"

sub_restore() {
    VERBOSE=0
    while [[ $# -gt 0 ]]
    do
    key="$1"
    case $key in
        "--host")
            _DB_HOST="$2"
            shift
            ;;
        "--port")
            _DB_PORT="$2"
            shift
            ;;
        "--user")
            _DB_USER="$2"
            shift
            ;;
        "--database")
            _DB_NAME="$2"
            shift
            ;;
        "--filename")
            _FILENAME="$2"
            shift
            ;;
        -v|--verbose)
            VERBOSE=1
            ;;
        -h|--help)
            _show_restore_help
            exit 1
            ;;
        *)
            echo "Missing command (-c|--command)"
            ;;
    esac
    shift # past argument or value
    done

    if [ "${_FILENAME}" == "" ]; then
        echo "Misssing filename (--filename)"
        echo ""
        _show_restore_help
        exit 1
    fi

    if [ ! -f "${_FILENAME}" ]; then
        echo "${_FILENAME} not found"
        exit 1
    fi

    if [ -f "${PG_PASS_FILE}" ]; then
        cp -v "${PG_PASS_FILE}" ~/.pgpass
    fi
    chmod 0600 ~/.pgpass

    FILE_TYPE=$(file -z "${_FILENAME}")
    #Check file compression type
    COMPRESSION_TYPE=""
    if [ "$(echo "${FILE_TYPE}" | grep -c 'compressed')" == "1" ]; then
        if [ "$(echo "${FILE_TYPE}" | grep -c 'bzip2 compressed')" == "1" ]; then
            COMPRESSION_TYPE="bzip2"
        else
            echo "Unsupported compression type: ${FILE_TYPE}"
            exit 1
        fi
    fi

    #Check dump type
    DUMP_TYPE=""
    if [ "$(echo "${FILE_TYPE}" | grep -c 'ASCII text')" == "1" ]; then
        DUMP_TYPE="PLAIN"
    elif [ "$(echo "${FILE_TYPE}" | grep -c 'UTF-8 Unicode text')" == "1" ]; then
        DUMP_TYPE="PLAIN"
    elif [ "$(echo "${FILE_TYPE}" | grep -c 'PostgreSQL custom database dump')" == "1" ]; then
        DUMP_TYPE="PostgreSQL custom"
    else
        echo "Unsupported file type: ${FILE_TYPE}"
            echo "Expected ASCII text (.sql), UTF-8 Unicode text (.sql) or PostgreSQL custom database dump (.sqlz)"
        exit 1
    fi

    echo "Restore:"
    echo "  file       : ${_FILENAME}"
    echo "  compression: ${COMPRESSION_TYPE}"
    echo "  dump type  : ${DUMP_TYPE}"

    #Decompress if needed
    if [ "${COMPRESSION_TYPE}" == "bzip2" ]; then
       echo "Decompressing backup: ${_FILENAME} --> ${_FILENAME%.*}"
       #COMPRESSED=1
       bunzip2 "${_FILENAME}"
       _FILENAME="${_FILENAME%.*}"
    fi

    echo "Recreating schema: public"
    psql --username "${_DB_USER}" --host="${_DB_HOST}" --port="${_DB_PORT}" -c "DROP SCHEMA public CASCADE;" "${_DB_NAME}"
    psql --username "${_DB_USER}" --host="${_DB_HOST}" --port="${_DB_PORT}" -c "CREATE SCHEMA public;" "${_DB_NAME}"
    psql --username "${_DB_USER}" --host="${_DB_HOST}" --port="${_DB_PORT}" -c "GRANT ALL ON SCHEMA public TO postgres;" "${_DB_NAME}"
    psql --username "${_DB_USER}" --host="${_DB_HOST}" --port="${_DB_PORT}" -c "GRANT ALL ON SCHEMA public TO ${_DB_USER};" "${_DB_NAME}"

    echo "Restoring: ${_FILENAME}"
    if [ "${DUMP_TYPE}" == "PLAIN" ]; then
        psql --no-password \
        --username "${_DB_USER}" \
        --host="${_DB_HOST}" \
        --port="${_DB_PORT}" \
        --file="${_FILENAME}" \
        "${_DB_NAME}"
    elif [ "${DUMP_TYPE}" == "PostgreSQL custom" ]; then
        pg_restore \
        --verbose \
        --no-password \
        --username "${_DB_USER}" \
        --host="${_DB_HOST}" \
        --port="${_DB_PORT}" \
        --dbname="${_DB_NAME}" \
        "${_FILENAME}"
    fi

    #Compress again if needed
    if [ "${COMPRESSION_TYPE}" == "bzip2" ]; then
        echo "Compressing again: ${_FILENAME} --> ${_FILENAME}.bz2"
        bzip2 "${_FILENAME}"
    fi
}

_show_restore_help() {
    echo "Usage: $ProgName restore [options]"
    echo ""
    echo "Restore a database dump. Tries to determine input file type."
    echo "bzip2 compressed backups are decompressed before restore and compressed after restore"
    echo "Plain text (.sql) dumps are restore using psql, PostgreSQL custom backups are restored using pg_restore"
    echo ""
    echo "Arguments:"
    echo "    --host <host>             Hostname to connect to"
    echo "    --port <port>             Port to connect to"
    echo "    --user <db user>          Database user"
    echo "    --database <db name>      Database name"
    echo "    --filename <filename>     Filename of the backup to restore"
    echo ""
}

sub_dump(){
    VERBOSE=0
    COMPRESS=1
    while [[ $# -gt 0 ]]
    do
    key="$1"
    case $key in
        "--host")
            _DB_HOST="$2"
            shift
            ;;
        "--port")
            _DB_PORT="$2"
            shift
            ;;
        "--user")
            _DB_USER="$2"
            shift
            ;;
        "--database")
            _DB_NAME="$2"
            shift
            ;;
        "--filename")
            _FILENAME="$2"
            shift
            ;;
        "--timestamp")
            _TIMESTAMP="$2"
            shift
            ;;
        "--no-compress")
            COMPRESS=0
            ;;
        -v|--verbose)
            VERBOSE=1
            ;;
        -h|--help)
            _show_dump_help
            exit 1
            ;;
        *)
            echo "Missing command (-c|--command)"
            ;;
    esac
    shift # past argument or value
    done

    if [ "${_FILENAME}" == "" ]; then
        echo "Error: Misssing filename (--filename)"
        echo ""
        _show_dump_help
        exit 1
    fi
    if [ "${_TIMESTAMP}" == "" ]; then
        _TIMESTAMP=$(date +%Y%m%dT%H%M%S)
    fi

    if [ -f "${PG_PASS_FILE}" ]; then
        cp -v "${PG_PASS_FILE}" ~/.pgpass
    fi
    chmod 0600 ~/.pgpass
    CMD_ARGS="-b"
    if [ "${VERBOSE}" == 1 ]; then
        CMD_ARGS="-b -e"
    fi

    COMPRESS_OPTS=""
    EXT=".sql"
    if [ "${COMPRESS}" == "1" ]; then
        #See: https://dan.langille.org/2013/06/10/using-compression-with-postgresqls-pg_dump/
        COMPRESS_OPTS="-Fc"
        EXT=".sqlz"
    fi

    pg_dump "${CMD_ARGS}" \
        --no-password \
        --username "${_DB_USER}" \
        --host="${_DB_HOST}" \
        --port="${_DB_PORT}" \
        ${COMPRESS_OPTS} "${_DB_NAME}" > "/backups/${_FILENAME}_${_TIMESTAMP}${EXT}"
}

_show_dump_help() {
    echo "Usage: $ProgName dump [options]"
    echo ""
    echo "Create a database dump. Pg_dump compression (-Fc) is enabled by default."
    echo "Output filename template: <filename>_<timestamp>.sql(z)"
    echo ""
    echo "Arguments:"
    echo "    --host <host>             Hostname to connect to"
    echo "    --port <port>             Port to connect to"
    echo "    --user <db user>          Database user"
    echo "    --database <db name>      Database name"
    echo "    --filename <filename>     Backup base filename. This will be appended with a timestamp and extension"
    echo "                              .sql for uncompressed backups and .sqlz for compressed backups"
    echo "    --timestamp <timestamp>   Timestamp to append to the backup file. If omitted, the current timestamp is"
    echo "                              used (YYYYMMDDTHHMMSS)"
    echo "    --no-compress             Do not use compression (which is enabled by default)"
    echo ""
}

sub_cmd(){
    VERBOSE=0
    while [[ $# -gt 0 ]]
    do
    key="$1"
    case $key in
        -c|--command)
            COMMAND="$2"
            shift
            ;;
        "--host")
            _DB_HOST="$2"
            shift
            ;;
        "--port")
            _DB_PORT="$2"
            shift
            ;;
        "--user")
            _DB_USER="$2"
            shift
            ;;
        "--database")
            _DB_NAME="$2"
            shift
            ;;
        -v|--verbose)
            VERBOSE=1
            ;;
        *)
            echo "Missing command (-c|--command)"
            ;;
    esac
    shift # past argument or value
    done

    if [ "${COMMAND}" == "" ]; then
        echo "Misssing command (-c|--command)"
        exit 1
    fi

    if [ -f "${PG_PASS_FILE}" ]; then
        cp -v "${PG_PASS_FILE}" ~/.pgpass
    fi
    chmod 0600 ~/.pgpass
    CMD_ARGS="-b"
    if [ "${VERBOSE}" == 1 ]; then
        CMD_ARGS="-b -e"
    fi
    # shellcheck disable=SC2086
    nice -n "${_NICE_LEVEL}" psql ${CMD_ARGS} -U "${_DB_USER}" --host="${_DB_HOST}" --port="${_DB_PORT}" -c "${COMMAND}" "${_DB_NAME}"
}

sub_file(){
    VERBOSE=0
    while [[ $# -gt 0 ]]
    do
    key="$1"
    case $key in
        -f|--file)
            FILE="$2"
            shift
            ;;
        "--host")
            _DB_HOST="$2"
            shift
            ;;
        "--port")
            _DB_PORT="$2"
            shift
            ;;
        "--user")
            _DB_USER="$2"
            shift
            ;;
        "--database")
            _DB_NAME="$2"
            shift
            ;;
        -v|--verbose)
            VERBOSE=1
            ;;
        *)
            echo "Missing file (-f|--file)"
            ;;
    esac
    shift # past argument or value
    done

    if [ "${FILE}" == "" ]; then
        echo "Misssing file (-f|--file)"
        exit 1
    fi

    if [ -f "${PG_PASS_FILE}" ]; then
        cp -v "${PG_PASS_FILE}" ~/.pgpass
    fi
    chmod 0600 ~/.pgpass
    CMD_ARGS="-b"
    if [ "${VERBOSE}" == 1 ]; then
        CMD_ARGS="-b -e"
    fi
    # shellcheck disable=SC2086
    nice -n "${_NICE_LEVEL}" psql ${CMD_ARGS} -U "${_DB_USER}" --host="${_DB_HOST}" --port="${_DB_PORT}" -f "${FILE}" "${_DB_NAME}"
}

sub_dir(){
    VERBOSE=0
    while [[ $# -gt 0 ]]
    do
    key="$1"
    case $key in
        -d|--dir)
            DIR="$2"
            shift
            ;;
        "--host")
            _DB_HOST="$2"
            shift
            ;;
        "--port")
            _DB_PORT="$2"
            shift
            ;;
        "--user")
            _DB_USER="$2"
            shift
            ;;
        "--database")
            _DB_NAME="$2"
            shift
            ;;
        -v|--verbose)
            VERBOSE=1
            ;;
        *)
            echo "Unsupported option: $key"
            ;;
    esac
    shift # past argument or value
    done

    if [ "${DIR}" == "" ]; then
        echo "Misssing dir (-d|--dir)"
        exit 1
    fi

    echo "Searching .sql files in ${DIR}"
    SQL_FILES=$(find "${DIR}" -name './*.sql')

    if [ "${SQL_FILES}" == "" ]; then
        echo "No .sql files found"
        echo "ls -l ${DIR}:"
        ls -la "${DIR}"
    else
        echo "Result:"
        while read -r filename
        do
            echo "  $filename"
        done <<< "${SQL_FILES}"

       echo "Processing:"
        while read -r filename
        do
            echo "  File: $filename"
            CMD_ARGS=""
            if [ "${VERBOSE}" == "1" ]; then
                CMD_ARGS="-v"
            fi
            sub_file "-f" "$filename" "${CMD_ARGS}" "--host" "${_DB_HOST}" "--port" "${_DB_PORT}" "--user" "${_DB_USER}" "--database" "${_DB_NAME}"
        done <<< "${SQL_FILES}"
    fi
}

sub_help(){
    echo "Usage: $ProgName <subcommand> [options]"
    echo ""
    echo "Arguments:"
    echo "    --host <host>             Database server hostname"
    echo "    --port <port>             Database server port"
    echo "    --user <db user>          Database username"
    echo "    --database <db name>      Database name"
    echo ""
    echo "Subcommands:"
    echo "    cmd       Run SQL command"
    echo "    file      Run SQL from file"
    echo "    dir       Run SQL from files in directory"
    echo "    dump      Make a SQL backup using pg_dump"
    echo "    restore   Resture a SQL backup"
    echo ""
    echo "For help with each subcommand run:"
    echo "$ProgName <subcommand> -h|--help"
    echo ""
}

#
# Process subcommands
#
subcommand=$1
case $subcommand in
    "" | "-h" | "--help")
        sub_help
        ;;
    *)
        shift
        sub_"${subcommand}" "$@"
        if [ $? = 127 ]; then
            echo "Error: '$subcommand' is not a known subcommand." >&2
            echo "       Run '$ProgName --help' for a list of known subcommands." >&2
            exit 1
        fi
        ;;
esac